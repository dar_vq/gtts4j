package com.tetradotoxina.gtts4j.service;

import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.io.File;
import java.io.IOException;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.tetradotoxina.gtts4j.service.impl.GTTSServiceImpl;



public class GTTSServiceTest {

	
	private GTTSService gttsService;
	
	
	@BeforeEach
	private void init() {
		gttsService=new GTTSServiceImpl();
	}
	
	
	@Test
	void shouldSaveAFileInTheSpecifiedPath() throws IOException {
		String text = "enter the text you want to convert to speech";
		String lang="en";	
		boolean slow = false;
		
		String filePath = System.getProperty("user.dir")+File.separator+"demo.mp3";
		
		byte[] data = gttsService.textToSpeech(text, lang, slow);
		gttsService.saveFile(filePath, data, true);
			
		assertNotNull(data);
	}

	@Test
	void shouldTranslateTheText() throws IOException {
		String text = "enter the text you want to translate";
		String lang="en";
		String langTranslate="es";
				
		String traslate = gttsService.translate(text, lang, langTranslate);
		System.out.println("Translation: "+traslate);	
		assertNotNull(traslate);
	}
	
}
