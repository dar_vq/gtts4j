package com.tetradotoxina.gtts4j.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;
import lombok.ToString;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
public class TranslationResponse {
	
	private String originalText;
	
	@Builder.Default
	private String originalLang = "auto";
	
	private String translatedText;	
	
	@Builder.Default
	private String langTranslation = "en"; 
	
	
}
