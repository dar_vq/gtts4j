package com.tetradotoxina.gtts4j.service;

import java.io.IOException;

import org.jsoup.Connection.Response;

import com.tetradotoxina.gtts4j.model.AttributeResponse;
import com.tetradotoxina.gtts4j.model.BatchExecuteRequest;
import com.tetradotoxina.gtts4j.model.TranslationRequest;
import com.tetradotoxina.gtts4j.model.ConfigTranslation;

public interface TranslationService {

	String textToSpeech(TranslationRequest translationRequest) throws IOException;

	AttributeResponse findAttributes(ConfigTranslation configTranslation) throws IOException;

	String translate(TranslationRequest translationRequest) throws IOException;
	
	Response postBatchExecute(BatchExecuteRequest batchExecuteRequest) throws IOException;
	
}
