package com.tetradotoxina.gtts4j.service.impl;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Base64;

import com.tetradotoxina.gtts4j.configuration.GTTSConstant;
import com.tetradotoxina.gtts4j.model.AttributeResponse;
import com.tetradotoxina.gtts4j.model.ConfigTranslation;
import com.tetradotoxina.gtts4j.model.TranslationRequest;
import com.tetradotoxina.gtts4j.service.GTTSService;
import com.tetradotoxina.gtts4j.service.TranslationService;

public class GTTSServiceImpl implements GTTSService {

	private TranslationService translationService;

	public GTTSServiceImpl() {
		translationService = new TraslateServiceImpl();
	}

	public String translate(String text, String lang,String langTranslate) throws IOException{

		ConfigTranslation traslateConfig = ConfigTranslation.builder()
				.text(text)
				.lang(lang)
				.langTranslate(langTranslate)
				.build();
		
		return translate(traslateConfig);
	}

	@Override
	public byte[] textToSpeech(String text) throws IOException {

		return textToSpeech(text, GTTSConstant.G_LANG_DEFAULT,GTTSConstant.G_LANG_TRANSLATE_DEFAULT, false);

	}
	
	@Override
	public byte[] textToSpeech(String text, boolean slow) throws IOException {

		return textToSpeech(text, GTTSConstant.G_LANG_DEFAULT,GTTSConstant.G_LANG_TRANSLATE_DEFAULT, slow);

	}
	
	@Override
	public byte[] textToSpeech(String text, String lang, boolean slow) throws IOException {

		return textToSpeech(text, lang,GTTSConstant.G_LANG_TRANSLATE_DEFAULT, slow);

	}
			
	@Override
	public byte[] textToSpeech(String text, String lang,String langTranslate, boolean slow) throws IOException {

		ConfigTranslation traslateConfig = ConfigTranslation.builder()
				.text(text)
				.lang(lang)
				.langTranslate(langTranslate)
				.build();
		
		return textToSpeech(traslateConfig);

	}
	
	@Override
	public byte[] textToSpeech(ConfigTranslation configTranslation) throws IOException {
			
		AttributeResponse attributeResponse = translationService.findAttributes(configTranslation);
			
		TranslationRequest translationRequest = TranslationRequest.builder()			
			.atParam(attributeResponse.getAtParam())
			.cookie(attributeResponse.getCookie())
			.fSid(attributeResponse.getFSid())
			.bl(attributeResponse.getBl())
			.text(configTranslation.getText().replaceAll("\n|\t|\r"," ").trim())
			.lang(configTranslation.getLang())
			.langTranslate(configTranslation.getLangTranslate())
			.reqid(attributeResponse.getReqid())
			.build();
	
	
		String response = translationService.textToSpeech(translationRequest);

		//System.out.println("Response: "+response);
	
		return getBytes(response);
		
	
	}

	private byte[] getBytes(String response) throws IOException {

		String src = "";
		byte[] bytes = null;

		try {
			String index = "//";
			String end = "\\\"]\",null,null,null,\"generic\"]]";
						
			int beginIndex = response.indexOf(index);
			int endIndex = response.lastIndexOf(end);
			
			src = response.substring(beginIndex, endIndex);
									
			bytes = Base64.getDecoder().decode(src);

		} catch (Exception e) {
			throw new IOException("Service response failed or file not found\n"+e.getMessage());
		}
				
		return bytes;

	}

	
	private String findTranslation(String response) throws IOException{
		
		//System.out.println("response: "+response);		
		
		//String index = "[[[null,null,null,null,null,[[\\\"";
		String index = "null,[[\\\"";
		String end = "\\\",null,null,null,[[";
		
					
		int beginIndex = response.indexOf(index)+index.length();
		int endIndex = response.lastIndexOf(end);
		
		String result = "";
				
		try {
			result = response.substring(beginIndex, endIndex);
		}catch (Exception e) {
			throw new IOException("No result found: "+e.getMessage());
		}
		
		return result;

	}
	
	@Override
	public void saveFile(String filePath, byte[] bytes) throws IOException {
		saveFile(filePath, bytes,false);
	}

	@Override
	public void saveFile(String filePath, byte[] bytes, boolean overwriteFile ) throws IOException {
		
		File file = new File(filePath);
		if (file.exists() && overwriteFile) {
			file.delete();
		}
		
		try (OutputStream stream = new FileOutputStream(filePath)) {
		    stream.write(bytes);
		}		
		
	}

	@Override
	public String translate(ConfigTranslation configTranslation) throws IOException {
			
		AttributeResponse attributeResponse = translationService.findAttributes(configTranslation);
		
		TranslationRequest translationRequest = TranslationRequest.builder()			
				.atParam(attributeResponse.getAtParam())
				.cookie(attributeResponse.getCookie())
				.fSid(attributeResponse.getFSid())
				.bl(attributeResponse.getBl())
				.text(configTranslation.getText().replaceAll("\r\n|\n|\t|\r"," ").trim())
				.lang(configTranslation.getLang())
				.langTranslate(configTranslation.getLangTranslate())
				.reqid(attributeResponse.getReqid())
				.build();
		
		String response = translationService.translate(translationRequest);
		
		return findTranslation(response);
	}


}
