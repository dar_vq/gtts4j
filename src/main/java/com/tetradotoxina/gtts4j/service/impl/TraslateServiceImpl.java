package com.tetradotoxina.gtts4j.service.impl;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.jsoup.Connection;
import org.jsoup.Connection.Method;
import org.jsoup.Connection.Response;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.tetradotoxina.gtts4j.configuration.GTTSConstant;
import com.tetradotoxina.gtts4j.model.AttributeResponse;
import com.tetradotoxina.gtts4j.model.BatchExecuteRequest;
import com.tetradotoxina.gtts4j.model.ConfigTranslation;
import com.tetradotoxina.gtts4j.model.TranslationRequest;
import com.tetradotoxina.gtts4j.service.TranslationService;


public class TraslateServiceImpl implements TranslationService {
	
	@Override
	public String textToSpeech(TranslationRequest traslateRequest) throws IOException {

		String url = GTTSConstant.URL_BASE
				+ String.format(GTTSConstant.URL_BATCH_EXECUTE, 
						traslateRequest.getFSid(), 
						traslateRequest.getBl(),
						traslateRequest.getLang(), 
						traslateRequest.getReqid());
				
		String fReqParam = String.format(GTTSConstant.REQUEST_PARAM_F_REQ_FORMAT,
				traslateRequest.getText(), 
				traslateRequest.getLang(),
				traslateRequest.getSlowFormat());

		
		BatchExecuteRequest batchExecuteRequest=BatchExecuteRequest.builder()
				.url(url)
				.cookie(traslateRequest.getCookie())
				.atParam(traslateRequest.getAtParam())
				.fReqParam(fReqParam)
				.build();
				
		Response response = postBatchExecute(batchExecuteRequest);
		
		return response.body();
	}

	@Override
	public AttributeResponse findAttributes(ConfigTranslation configTranslation) throws IOException {
		
		String url = GTTSConstant.URL_BASE + String.format(GTTSConstant.URL_PATH, 
				configTranslation.getLang(),
				configTranslation.getLang(),
				configTranslation.getLangTranslate(), 
				configTranslation.getText().replaceAll("\r\n|\n|\r","%0A").trim());
		
		//System.out.println("URL findAttributes: "+url);
		
		Response response = Jsoup.connect(url)
				.userAgent(GTTSConstant.USER_AGENT)
				.ignoreContentType(true)
				.method(Connection.Method.GET)
				.followRedirects(true)
				.execute();
			
		Document doc = Jsoup.parse(response.body());
		Elements scripts = doc.getElementsByTag("script");
		Map<String,String> params = findParameters(scripts);
		
		String cookie = response.cookie(GTTSConstant.COOKIE_NID);
		
		//String reqid="";
		
		return AttributeResponse.builder()
				.cookie(cookie)
				.atParam(params.get("at"))				
				.fSid(params.get("fsid"))
				.bl(configTranslation.getLangTranslate())
				//.reqid(reqid)
				.build();
	}
	
	private Map<String,String> findParameters(Elements scripts)throws IOException {
						
		Map<String,String> result = new HashMap<>();
		
	    for(Element script: scripts) {
	    	
	    	if(script.outerHtml().indexOf("window.WIZ_global_data") == -1) {
	    		continue;
	    	}
	    	
	    	//System.out.println(script.outerHtml());
	    				
			String atParam = "";
			String fsid = "";
			String bl="";
						
			//FdrFJe
			Pattern fsidPattern = Pattern.compile("\"FdrFJe\":\"(.*?)\"");
			Matcher fsidMatcher = fsidPattern.matcher(script.outerHtml());
			
			while(fsidMatcher.find()) {
				fsid=fsidMatcher.group(1);
			}
			
			
			//cfb2h
			Pattern blPattern = Pattern.compile("\"cfb2h\":\"(.*?)\"");
			Matcher blMatcher = blPattern.matcher(script.outerHtml());
			
			while(blMatcher.find()) {				
				bl=blMatcher.group(1);
			}
			
			
			//at
			Pattern atPattern = Pattern.compile("\"SNlM0e\":\"(.*?)\"");
			Matcher atMatcher = atPattern.matcher(script.outerHtml());
			
			while(atMatcher.find()) {				
				atParam = atMatcher.group(1);
			}
			
			//System.out.println("fsid: "+fsid);
			//System.out.println("bl: "+bl);
			//System.out.println("at: "+atParam);
			
			result.put("at", atParam);
			result.put("fsid", fsid);
			result.put("bl", bl);
			
			break;		
		}
				
		return result;
	}

	@Override
	public String translate(TranslationRequest traslateRequest) throws IOException {

		String url = GTTSConstant.URL_BASE
				+ String.format(GTTSConstant.URL_TRASLATE_BATCH_EXECUTE, 
						traslateRequest.getFSid(), 
						traslateRequest.getBl(),
						traslateRequest.getLang(), 
						traslateRequest.getReqid());
				
		String fReqParam = String.format(GTTSConstant.REQUEST_TRASLATE_PARAM_F_REQ_FORMAT,
				traslateRequest.getText(), 
				traslateRequest.getLang(),
				traslateRequest.getLangTranslate());
		
		BatchExecuteRequest batchExecuteRequest=BatchExecuteRequest.builder()
				.url(url)
				.cookie(traslateRequest.getCookie())
				.atParam(traslateRequest.getAtParam())
				.fReqParam(fReqParam)
				.build();
				
		Response response = postBatchExecute(batchExecuteRequest);
		
		return response.body();
	}

	

	@Override
	public Response postBatchExecute(BatchExecuteRequest batchExecuteRequest) throws IOException {
		
		/*System.out.println("url: "+batchExecuteRequest.getUrl());
		System.out.println(GTTSConstant.HEADER_CONTENT_TYPE+": "+batchExecuteRequest.getContentType());
		System.out.println(GTTSConstant.REQUEST_PARAM_F_REQ+": "+batchExecuteRequest.getFReqParam());
		System.out.println(GTTSConstant.REQUEST_PARAM_AT+" :"+ batchExecuteRequest.getAtParam());
		System.out.println(GTTSConstant.COOKIE_NID+" :"+ batchExecuteRequest.getCookie());*/
		
		Connection.Response res = Jsoup.connect(batchExecuteRequest.getUrl())
				.userAgent(batchExecuteRequest.getUserAgent())
				.header(GTTSConstant.HEADER_CONTENT_TYPE, batchExecuteRequest.getContentType())
				.data(GTTSConstant.REQUEST_PARAM_F_REQ, batchExecuteRequest.getFReqParam())
				.data(GTTSConstant.REQUEST_PARAM_AT, batchExecuteRequest.getAtParam())
				.cookie(GTTSConstant.COOKIE_NID, batchExecuteRequest.getCookie())
				.method(Method.POST)
				.ignoreContentType(true)
				.execute();

		if (res.statusCode() != 200) {
			throw new IOException("statusCode: " + res.statusCode());
		}

		return res;		
	}
}
