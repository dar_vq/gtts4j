package com.tetradotoxina.gtts4j.service;

import java.io.IOException;

import com.tetradotoxina.gtts4j.model.ConfigTranslation;

public interface GTTSService {	
	
	byte[] textToSpeech(String text)throws IOException;
	
	byte[] textToSpeech(String text,boolean slow) throws IOException;
	
	byte[] textToSpeech(String text,String lang,boolean slow) throws IOException;
	
	byte[] textToSpeech(String text, String lang,String langTranslate, boolean slow) throws IOException;
	
	byte[] textToSpeech(ConfigTranslation configTranslation) throws IOException;
	
	void saveFile(String filePath,byte[] bytes) throws IOException;
	
	void saveFile(String filePath,byte[] bytes,boolean overwriteFile) throws IOException;
	
	String translate(String text, String lang,String langTranslate) throws IOException;
	
	String translate(ConfigTranslation configTranslation) throws IOException;
}
