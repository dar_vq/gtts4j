package com.tetradotoxina.gtts4j.configuration;

public class GTTSConstant {

	public static String USER_AGENT = "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.114 Safari/537.36";
	
	public static String URL_BASE = "https://translate.google.com";
	
	public static String URL_PATH = "/?hl=%s&sl=%s&tl=%s&text=%s&op=translate";
	
	public static final String URL_BATCH_EXECUTE = "/_/TranslateWebserverUi/data/batchexecute?rpcids=jQ1olc&f.sid=%s&bl=%s&hl=%s&soc-app=1&soc-platform=1&soc-device=1&_reqid=%s&rt=c";
	
	public static final String HEADER_CONTENT_TYPE = "Content-Type";
	public static final String HEADER_CONTENT_TYPE_FORM = "application/x-www-form-urlencoded;charset=UTF-8";
	
	public static final String COOKIE_NID = "NID";
	
	public static final String REQUEST_PARAM_F_REQ = "f.req";
	public static final String REQUEST_PARAM_AT = "at";

	
	public static final String REQUEST_PARAM_F_REQ_FORMAT = "[[[\"jQ1olc\",\"[\\\"%s\\\",\\\"%s\\\",%s,\\\"null\\\"]\",null,\"generic\"]]]";
	
	public static final String G_LANG_DEFAULT = "auto";
	public static final String G_LANG_TRANSLATE_DEFAULT = "en";
	
	
	public static String URL_TRASLATE_BATCH_EXECUTE = "/_/TranslateWebserverUi/data/batchexecute?rpcids=MkEWBc&f.sid=%s&bl=%s&hl=%s&soc-app=1&soc-platform=1&soc-device=1&_reqid=%s&rt=c";
	public static String REQUEST_TRASLATE_PARAM_F_REQ_FORMAT="[[[\"MkEWBc\",\"[[\\\"%s\\\",\\\"%s\\\",\\\"%s\\\",true],[null]]\",null,\"generic\"]]]";
	
	private GTTSConstant() {};
}
