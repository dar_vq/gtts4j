# gTTS4J
gTTS4j (Google Text-to-Speech for Java),  This library is inspired by the [gtts](https://github.com/pndurette/gTTS) library made in python.
Convert text to speech using the Google API by returning an mp3 file or you can manipulate the audio bits as well. When working with the Google API I have also integrated the translation

## Social networks
* [Web](https://bit.ly/3wBoEea)
* [Twitch](https://bit.ly/3vsWIZw)
* [Youtube](https://www.youtube.com/channel/UCKGueVQMO7_YhifvFuRBb6g)
* [Fanpage Oficial](https://bit.ly/3wHqavn)
* [Fanpage Developers](https://bit.ly/3upIG9V)
* [Facebook Group](https://bit.ly/3bTZaAQ)

## Disclaimer
This project is not affiliated with Google or Google Cloud. In the future there may be changes that make the library obsolete. 

## _Text to Speech_
	GTTSService gttsService = new GTTSServiceImpl();
	String text = "enter the text you want to convert to speech";
	String lang = "en";
	boolean slow = false;
	String filePath = System.getProperty("user.dir")+File.separator+"demo.mp3";
	
	byte[] data = gttsService.textToSpeech(text, lang, slow);
	gttsService.saveFile(filePath, data, true);

## _Translate_	
	GTTSService gttsService = new GTTSServiceImpl();
	String text = "enter the text you want to translate";
	String lang="en";
	String langTranslate="es";
	
	String traslate = gttsService.translate(text, lang, langTranslate);
	System.out.println("Translation: "+traslate);

## Supported Languages 

  * 'af' : 'Afrikaans'
  * 'sq' : 'Albanian'
  * 'ar' : 'Arabic'
  * 'hy' : 'Armenian'
  * 'bn' : 'Bengali'
  * 'ca' : 'Catalan'
  * 'zh' : 'Chinese'
  * 'zh-cn' : 'Chinese (Mandarin/China)'
  * 'zh-yue' : 'Chinese (Cantonese)'
  * 'hr' : 'Croatian'
  * 'cs' : 'Czech'
  * 'da' : 'Danish'
  * 'nl' : 'Dutch'
  * 'en' : 'English'
  * 'eo' : 'Esperanto'
  * 'fi' : 'Finnish'
  * 'fr' : 'French'
  * 'de' : 'German'
  * 'el' : 'Greek'
  * 'hi' : 'Hindi'
  * 'hu' : 'Hungarian'
  * 'is' : 'Icelandic'
  * 'id' : 'Indonesian'
  * 'it' : 'Italian'
  * 'ja' : 'Japanese'
  * 'km' : 'Khmer (Cambodian)'
  * 'ko' : 'Korean'
  * 'la' : 'Latin'
  * 'lv' : 'Latvian'
  * 'mk' : 'Macedonian'
  * 'no' : 'Norwegian'
  * 'pl' : 'Polish'
  * 'pt' : 'Portuguese'
  * 'ro' : 'Romanian'
  * 'ru' : 'Russian'
  * 'sr' : 'Serbian'
  * 'si' : 'Sinhala'
  * 'sk' : 'Slovak'
  * 'es' : 'Spanish'
  * 'sw' : 'Swahili'
  * 'sv' : 'Swedish'
  * 'ta' : 'Tamil'
  * 'th' : 'Thai'
  * 'tr' : 'Turkish'
  * 'uk' : 'Ukrainian'
  * 'vi' : 'Vietnamese'
  * 'cy' : 'Welsh'
